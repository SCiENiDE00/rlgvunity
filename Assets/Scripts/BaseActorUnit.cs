﻿using System;
using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;
using RLG.Assets.Scripts.Contracts;
using RLG.Assets.Scripts.Models;

public abstract class BaseActorUnit : MonoBehaviour, ICombatant, ICorrespondent
{
    public event EventHandler<MessageArgument> DataPublisher;

    public float corpseRemainDelay = 20f;
    public float moveTime = 0.1f;
    private int blockingLayer;
    public ActorConfigurationData configuration;
    public ActorStateData stateData;

    private float inverseMoveTime;
    private BoxCollider2D boxCollider;
    private Rigidbody2D rb2d;

    public void Attack<T>(T target) where T : ICombatant
    {
        float damage = Random.Range(stateData.strength + stateData.agility * 0.55f,
            (stateData.strength * 1.15f) * Mathf.Sqrt(Mathf.Ceil((stateData.intellect * 0.45f + stateData.agility * 0.85f) / configuration.level)));

        target.TakeDamage(damage);

        PublishData(string.Format("You hit {0} for {1} damage!", target.name, Mathf.RoundToInt(damage)));
    }
    public void TakeDamage(float damage)
    {
        float damageReduction = Random.Range(Mathf.Log10(stateData.agility + stateData.intellect * 0.9f + stateData.armor),
            Mathf.Sqrt(stateData.agility * 0.75f + stateData.intellect * 0.65f + stateData.armor));
        float directDamage = (damage - damageReduction);

        stateData.health -= directDamage;

        PublishData(string.Format("Someone hits you for {0} damage!", Mathf.RoundToInt(directDamage)));

        CheckIsDead();
    }
    public void PublishData(string data)
    {
        MessageArgument message = (MessageArgument)Activator.CreateInstance(typeof(MessageArgument), new object[] { data });
        OnDataPublisher(message);
    }

    protected bool Move(int xDir, int yDir, out RaycastHit2D hit)
    {
        Vector2 start = transform.position;
        Vector2 end = start + new Vector2(xDir, yDir);

        boxCollider.enabled = false;
        hit = Physics2D.Linecast(start, end, blockingLayer);
        boxCollider.enabled = true;

        if (hit.transform == null)
        {
            StartCoroutine(SmoothMovement(end));
            return true;
        }

        return false;
    }
    protected IEnumerator SmoothMovement(Vector3 end)
    {
        float sqrRemainingDistance = (transform.position - end).sqrMagnitude;
        while (sqrRemainingDistance > float.Epsilon)
        {
            Vector3 newPosition = Vector3.MoveTowards(rb2d.position, end, inverseMoveTime * Time.deltaTime);
            rb2d.MovePosition(newPosition);
            sqrRemainingDistance = (transform.position - end).sqrMagnitude;
            yield return null;
        }
    }

    protected virtual void Start()
    {
        boxCollider = GetComponent<BoxCollider2D>();
        rb2d = GetComponent<Rigidbody2D>();
        inverseMoveTime = 1 / moveTime;

        blockingLayer = 1 << LayerMask.NameToLayer("Unit") | 1 << LayerMask.NameToLayer("Static");

        configuration = new ActorConfigurationData();
        stateData = new ActorStateData(configuration);

        DataPublisher += BaseActorUnit_Correspondence;
    }
    protected virtual void AttemptMove<T>(int xDir, int yDir)
        where T : Component
    {
        RaycastHit2D hit;
        bool canMove = Move(xDir, yDir, out hit);

        if (hit.transform == null)
            return;

        T hitComponent = hit.transform.GetComponent<T>();

        if (!canMove && hitComponent != null)
            OnCantMove(hitComponent);
    }
    protected virtual void CheckIsDead()
    {
        if (stateData.health <= 0)
        {
            GameObject[] bloodTiles = GameManager.instance.boardScript.specialTiles.bloodTiles;
            GameObject toInstantiate = bloodTiles[Random.Range(0, bloodTiles.Length)];
            GameObject corpse = Instantiate(toInstantiate, transform.position, Quaternion.identity);

            Destroy(corpse, corpseRemainDelay);

            if (gameObject.tag == "Player")
            {
                Camera.main.transform.parent = null;
                GameManager.instance.GameOver();
            }
            else
            {
                GameManager.instance.RemoveEnemyFromList(this as Enemy);
            }

            gameObject.SetActive(false);
            Destroy(gameObject, 2.5f);
        }
    }

    protected abstract void OnCantMove<T>(T component) where T : Component;
    protected abstract void BaseActorUnit_Correspondence(object sender, MessageArgument e);

    private void OnDataPublisher(MessageArgument args)
    {
        DataPublisher?.Invoke(this, args);
    }
}
