﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class BoardManager : MonoBehaviour
{
    public int columns = 8;
    public int rows = 8;
    public Count wallCount = new Count(5, 9);
    public Count foodCount = new Count(1, 5);
    public Count enemyCount = new Count(1, 3);
    public SpecialTiles specialTiles = new SpecialTiles();
    public UnitTiles unitTiles = new UnitTiles();
    public TerrainTiles terrainTiles = new TerrainTiles();

    private Transform boardHolder;
    private List<Vector3> gridPositions = new List<Vector3>();

    public void SetupScene(int level)
    {
        BoardSetup();
        InitializeList();
        LayoutObjectAtRandom(terrainTiles.wallTiles, wallCount.mininum, wallCount.maxinum);
        LayoutObjectAtRandom(unitTiles.enemyTiles, enemyCount.mininum, enemyCount.maxinum);

        Instantiate(specialTiles.nextLevel, RandomPositon(), Quaternion.identity);
        if (level > 1)
        {
            Instantiate(specialTiles.prevLevel, RandomPositon(), Quaternion.identity);
        }
    }

    private void InitializeList()
    {
        gridPositions.Clear();

        for (int x = 1; x < columns - 1; x++)
        {
            for (int y = 1; y < rows - 1; y++)
            {
                gridPositions.Add(new Vector3(x, y, 0f));
            }
        }
    }
    private void BoardSetup()
    {
        boardHolder = new GameObject("Board").transform;

        for (int x = -1; x < columns + 1; x++)
        {
            for (int y = -1; y < rows + 1; y++)
            {
                GameObject toInstantiate = terrainTiles.floorTiles[Random.Range(0, terrainTiles.floorTiles.Length)];
                if (x == -1 || x == columns || y == -1 || y == rows)
                {
                    toInstantiate = terrainTiles.wallTiles[Random.Range(0, terrainTiles.wallTiles.Length)];
                }

                GameObject instance = Instantiate(toInstantiate, new Vector3(x, y, 0f), Quaternion.identity);
                instance.transform.SetParent(boardHolder);                
            }
        }
    }
    private void LayoutObjectAtRandom(GameObject[] tileArray, int min, int max)
    {
        int objectCount = Random.Range(min, max + 1);

        for (int i = 0; i < objectCount; i++)
        {
            Vector3 randomPosition = RandomPositon();
            GameObject tile = tileArray[Random.Range(0, tileArray.Length)];
            GameObject instance = Instantiate(tile, randomPosition, Quaternion.identity);
            instance.transform.SetParent(boardHolder);
        }
    }
    private Vector3 RandomPositon()
    {
        int randomIdx = Random.Range(0, gridPositions.Count);
        Vector3 randomPosition = gridPositions[randomIdx];
        gridPositions.RemoveAt(randomIdx);

        return randomPosition;
    }

    [Serializable]
    public class SpecialTiles
    {
        public GameObject nextLevel;
        public GameObject prevLevel;
        public GameObject[] bloodTiles;
    }
    [Serializable]
    public class UnitTiles
    {
        public GameObject[] enemyTiles;
    }
    [Serializable]
    public class TerrainTiles
    {
        public GameObject[] floorTiles;
        public GameObject[] wallTiles;
    }
    [Serializable]
    public class Count
    {
        public int mininum;
        public int maxinum;

        public Count(int min, int max)
        {
            mininum = min;
            maxinum = max;
        }
    }
}
