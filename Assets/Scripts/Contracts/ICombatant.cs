﻿namespace RLG.Assets.Scripts.Contracts
{
    public interface ICombatant
    {
        string name { get; set; }
        void Attack<T>(T target) where T : ICombatant;
        void TakeDamage(float damage);
    }
}
