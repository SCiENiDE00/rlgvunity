﻿using System;
using RLG.Assets.Scripts.Models;

namespace RLG.Assets.Scripts.Contracts
{
    public interface ICorrespondent
    {
        event EventHandler<MessageArgument> DataPublisher;
        void PublishData(string data);
    }
}
