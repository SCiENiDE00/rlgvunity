﻿using UnityEngine;
using RLG.Assets.Scripts.Contracts;
using RLG.Assets.Scripts.Models;

public class Enemy : BaseActorUnit
{
    private bool skipMove;
    private Transform target;

    public void MoveEnemy()
    {
        int xDir = 0;
        int yDir = 0;

        //if (Mathf.Abs(target.position.x - transform.position.x) < float.Epsilon)
        //{
        //    yDir = target.position.y > transform.position.y ? 1 : -1;
        //}
        //else
        //{
        //    xDir = target.position.x > transform.position.x ? 1 : -1;
        //}

        switch (Random.Range(0, 4)) //0,1,2
        {
            case 0:
                xDir = -1;
                yDir = 0;
                break;
            case 1:
                xDir = 0;
                yDir = -1;
                break;
            case 2:
                xDir = 1;
                yDir = 0;
                break;
            case 3:
                xDir = 0;
                yDir = 1;
                break;
            default:
                throw new System.IndexOutOfRangeException();
        }

        AttemptMove<BaseActorUnit>(xDir, yDir);
    }

    protected override void Start()
    {
        GameManager.instance.AddEnemyToList(this);
        target = GameObject.FindGameObjectWithTag("Player").transform;
        base.Start();
    }
    protected override void AttemptMove<T>(int xDir, int yDir)
    {
        if (skipMove)
        {
            skipMove = false;
            return;
        }

        base.AttemptMove<T>(xDir, yDir);
        skipMove = true;
    }
    protected override void OnCantMove<T>(T component)
    {
        ICombatant target = component as ICombatant;
        if (target != null)
        {
            Attack(target); 
        }
    }
    protected override void BaseActorUnit_Correspondence(object sender, MessageArgument e)
    {
    }
}
