﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class FieldOfView : MonoBehaviour
{
    [Range(0, 360)]
    public float viewAngle;
    public float viewRadius;
    public float meshResolution;
    public float edgeDstThreshold;
    public float maskCutawayDist = .1f;
    public int edgeResolveIterations;
    [HideInInspector]
    public List<Transform> visibleTargets = new List<Transform>();
    [HideInInspector]
    public List<Transform> oldVisibleTargets = new List<Transform>();
    public MeshFilter viewMeshFilter;

    private int allExceptTerrainMask;
    private int obstacleMask;
    private Shader defaultSpriteShader;
    private Shader stencilSpriteShader;
    private Mesh viewMesh;

    public Vector3 DirFromAngle(float angleInDegrees, bool angleIsGlobal)
    {
        if (!angleIsGlobal)
        {
            angleInDegrees += transform.eulerAngles.y;
        }

        return new Vector3(Mathf.Sin(angleInDegrees * Mathf.Deg2Rad), Mathf.Cos(angleInDegrees * Mathf.Deg2Rad), 0);
    }

    private void Start()
    {
        allExceptTerrainMask = ~(1 << LayerMask.NameToLayer("Terrain"));
        obstacleMask = 1 << LayerMask.NameToLayer("Static");

        viewMesh = new Mesh();
        viewMesh.name = "View Mesh";
        viewMeshFilter.mesh = viewMesh;

        defaultSpriteShader = Shader.Find("Sprites/Default");
        stencilSpriteShader = Shader.Find("Custom/SpritesStencil");

        StartCoroutine("FindTargetWithDelay", .2f);
    }
    private void LateUpdate()
    {
        DrawFieldOfView();
    }
    private void FindVisibleTargets(int targetMask)
    {
        visibleTargets.Clear();
        Collider2D[] targetsInViewRadius = Physics2D.OverlapCircleAll(transform.position, viewRadius, targetMask);
        for (int i = 0; i < targetsInViewRadius.Length; i++)
        {
            Transform target = targetsInViewRadius[i].transform;
            // Skip the player, no point of processing him
            if (target.tag == "Player") continue;

            Vector3 dirToTarget = (target.position - transform.position).normalized;
            if (Vector3.Angle(transform.up, dirToTarget) <= viewAngle / 2)
            {
                float dstToTarget = Vector3.Distance(transform.position, target.position);
                if (!Physics2D.Raycast(transform.position, dirToTarget, dstToTarget, obstacleMask))
                {
                    visibleTargets.Add(target);
                }
            }
        }

        UpdateUnitsInFoV();
    }
    private void UpdateUnitsInFoV()
    {
        bool shouldClear = false;
        List<Transform> targetsOutOfView = oldVisibleTargets.Except(visibleTargets).ToList();
        if (targetsOutOfView.Count > 0)
        {
            for (int i = 0; i < targetsOutOfView.Count; i++)
            {
                if (targetsOutOfView[i] == null)
                {
                    targetsOutOfView.RemoveAt(i);
                    continue;
                }

                SpriteRenderer renderer = targetsOutOfView[i].GetComponent<SpriteRenderer>();
                renderer.material.shader = stencilSpriteShader;
            }

            shouldClear = true;
        }

        visibleTargets = visibleTargets.Except(oldVisibleTargets).ToList();
        if (shouldClear)
            oldVisibleTargets.Clear();

        for (int j = 0; j < visibleTargets.Count; j++)
        {
            if (visibleTargets[j] == null)
            {
                visibleTargets.RemoveAt(j);
                continue;
            }

            Transform target = visibleTargets[j];
            if ((target.gameObject.layer == 10 || target.gameObject.layer == 12) && target.tag != "Player")
            {
                SpriteRenderer renderer = target.GetComponent<SpriteRenderer>();
                renderer.material.shader = defaultSpriteShader;
                oldVisibleTargets.Add(target);
            }
        }
    }
    private void DrawFieldOfView()
    {
        int stepCount = Mathf.RoundToInt(viewAngle * meshResolution);
        float stepAngleSize = viewAngle / stepCount;
        List<Vector3> viewPoints = new List<Vector3>();
        ViewCastInfo oldViewCast = new ViewCastInfo();

        for (int i = 0; i <= stepCount; i++)
        {
            float angle = transform.eulerAngles.y - viewAngle / 2 + stepAngleSize * i;
            ViewCastInfo newViewCast = ViewCast(angle);
            if (i > 0)
            {
                bool edgeDstThresholdExceeded = Mathf.Abs(oldViewCast.distance - newViewCast.distance) > edgeDstThreshold;
                if (oldViewCast.hit != newViewCast.hit || (oldViewCast.hit && newViewCast.hit && edgeDstThresholdExceeded))
                {
                    EdgeInfo edge = FindEdge(oldViewCast, newViewCast);
                    if (edge.pointA != Vector3.zero)
                    {
                        viewPoints.Add(edge.pointA);
                    }
                    if (edge.pointB != Vector3.zero)
                    {
                        viewPoints.Add(edge.pointB);
                    }
                }
            }

            viewPoints.Add(newViewCast.point);
            oldViewCast = newViewCast;
        }

        int vertexCount = viewPoints.Count + 1;
        Vector3[] vertices = new Vector3[vertexCount];
        int[] triangles = new int[(vertexCount - 2) * 3];

        vertices[0] = Vector3.zero;
        for (int i = 0; i < vertexCount - 1; i++)
        {
            Vector3 tempPoint = transform.InverseTransformPoint(viewPoints[i]);
            vertices[i + 1] = tempPoint + tempPoint.normalized * maskCutawayDist;

            if (i < vertexCount - 2)
            {
                triangles[i * 3] = 0;
                triangles[i * 3 + 1] = i + 1;
                triangles[i * 3 + 2] = i + 2;
            }
        }

        viewMesh.Clear();
        viewMesh.vertices = vertices;
        viewMesh.triangles = triangles;
        viewMesh.RecalculateNormals();
    }
    private IEnumerator FindTargetWithDelay(float delay)
    {
        while (true)
        {
            yield return new WaitForSeconds(delay);
            FindVisibleTargets(allExceptTerrainMask);
        }
    }
    private EdgeInfo FindEdge(ViewCastInfo minViewCast, ViewCastInfo maxViewCast)
    {
        float minAngle = minViewCast.angle;
        float maxAngle = maxViewCast.angle;
        Vector3 minPoint = Vector3.zero;
        Vector3 maxPoint = Vector3.zero;

        for (int i = 0; i < edgeResolveIterations; i++)
        {
            float angle = (minAngle + maxAngle) / 2;
            ViewCastInfo newViewCast = ViewCast(angle);
            bool edgeDstThresholdExceeded = Mathf.Abs(minViewCast.distance - newViewCast.distance) > edgeDstThreshold;
            if (newViewCast.hit == minViewCast.hit && !edgeDstThresholdExceeded)
            {
                minAngle = angle;
                minPoint = newViewCast.point;
            }
            else
            {
                maxAngle = angle;
                maxPoint = newViewCast.point;
            }
        }

        return new EdgeInfo(minPoint, maxPoint);
    }
    private ViewCastInfo ViewCast(float globalAngle)
    {
        Vector3 dir = DirFromAngle(globalAngle, true);
        RaycastHit2D hit = Physics2D.Raycast(transform.position, dir, viewRadius, obstacleMask);
        ViewCastInfo viewCastInfo;
        if (hit)
        {
            viewCastInfo = new ViewCastInfo(true, hit.point, hit.distance, globalAngle);
        }
        else
        {
            viewCastInfo = new ViewCastInfo(false, transform.position + dir * viewRadius, viewRadius, globalAngle);
        }

        return viewCastInfo;
    }

    public struct ViewCastInfo
    {
        public bool hit;
        public Vector3 point;
        public float distance;
        public float angle;

        public ViewCastInfo(bool _hit, Vector3 _point, float _dist, float _angle)
        {
            hit = _hit;
            point = _point;
            distance = _dist;
            angle = _angle;
        }
    }
    public struct EdgeInfo
    {
        public Vector3 pointA;
        public Vector3 pointB;

        public EdgeInfo(Vector3 a, Vector3 b)
        {
            pointA = a;
            pointB = b;
        }
    }
}
