﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;

    [HideInInspector]
    public bool playersTurn = true;
    public float levelStartDelay = 1f;
    public float turnDelay = .1f;
    public BoardManager boardScript;
    [HideInInspector]
    public int level = 0;

    private bool enemiesMoving;
    private bool doingSetup;
    private Text levelText;
    private GameObject levelImage;
    private List<Enemy> enemies;
    private List<Enemy> waitingForRemoval;

    public void AddEnemyToList(Enemy enemy)
    {
        enemies.Add(enemy);
    }
    public void RemoveEnemyFromList(Enemy enemy)
    {
        if (enemiesMoving)
        {
            waitingForRemoval.Add(enemy);
            return;
        }

        if (enemy != null)
            enemies.Remove(enemy);
    }
    public void GameOver()
    {
        levelText.text = "You died on level " + level;
        levelImage.SetActive(true);
        enabled = false;
    }

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
        enemies = new List<Enemy>();
        waitingForRemoval = new List<Enemy>();
        boardScript = GetComponent<BoardManager>();
    }
    private void Update()
    {
        if (playersTurn || enemiesMoving || doingSetup)
            return;

        StartCoroutine(MoveEnemies());
    }
    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }
    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnLevelFinishedLoading;
    }
    private void InitGame()
    {
        doingSetup = true;

        levelImage = GameObject.Find("LevelImage");
        levelText = GameObject.Find("LevelText").GetComponent<Text>();
        levelText.text = "Level " + level;
        levelImage.SetActive(true);
        Invoke("HideLevelImage", levelStartDelay);

        enemies.Clear();
        boardScript.SetupScene(level);
    }
    private void OnLevelFinishedLoading(Scene arg0, LoadSceneMode arg1)
    {
        level++;
        InitGame();
    }
    private void HideLevelImage()
    {
        levelImage.SetActive(false);
        doingSetup = false;
    }
    private IEnumerator MoveEnemies()
    {
        enemiesMoving = true;
        yield return new WaitForSeconds(turnDelay);
        if (enemies.Count == 0)
        {
            yield return new WaitForSeconds(turnDelay);
        }

        for (int i = 0; i < enemies.Count; i++)
        {
            if (enemies[i] == null)
            {
                Debug.Log("YIELD BREAK");
                yield break;
            }

            enemies[i].MoveEnemy();
            yield return new WaitForSeconds(enemies[i].moveTime);
        }

        playersTurn = true;
        enemiesMoving = false;

        if (waitingForRemoval.Count > 0)
        {
            foreach (Enemy enemy in waitingForRemoval)
            {
                RemoveEnemyFromList(enemy);
            }

            waitingForRemoval.Clear();
        }
    }
}
