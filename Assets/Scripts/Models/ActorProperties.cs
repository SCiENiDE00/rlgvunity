﻿using System;
using Random = UnityEngine.Random;

namespace RLG.Assets.Scripts.Models
{
    public class ActorConfigurationData
    {
        public int level;
        public float baseHealth;
        public float baseArmor;
        public float baseStrength;
        public float baseAgility;
        public float baseIntellect;
        public float baseStamina;

        public ActorConfigurationData()
        {
            level = 1;
            baseHealth = Random.Range(16, 23);
            baseArmor = Random.Range(5, 10);
            baseStrength = Random.Range(2, 4);
            baseAgility = Random.Range(2, 4);
            baseIntellect = Random.Range(2, 4);
            baseStamina = Random.Range(2, 4);
        }
    }
    public class ActorStateData
    {
        public float health;
        public float armor;
        public float strength;
        public float agility;
        public float intellect;
        public float stamina;

        public ActorStateData(ActorConfigurationData config)
        {
            health = config.baseHealth;
            armor = config.baseArmor;
            strength = config.baseStrength;
            agility = config.baseAgility;
            intellect = config.baseIntellect;
            stamina = config.baseStamina;
        }
    }
}
