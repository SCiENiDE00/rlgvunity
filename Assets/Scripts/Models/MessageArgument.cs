﻿using System;

namespace RLG.Assets.Scripts.Models
{
    public class MessageArgument : EventArgs
    {
        public string Message { get; private set; }
        public MessageArgument(string message)
        {
            Message = message;
        }
    }
}
