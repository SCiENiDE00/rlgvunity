﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using RLG.Assets.Scripts.Contracts;
using RLG.Assets.Scripts.Models;
using System.Collections;

public class Player : BaseActorUnit
{
    public float restartLevelDelay = .25f;
    public Text infoText;

    protected override void Start()
    {
        base.Start();
    }
    protected override void AttemptMove<T>(int xDir, int yDir)
    {
        base.AttemptMove<T>(xDir, yDir);

        RaycastHit2D hit;
        if (Move(xDir, yDir, out hit)) { }

        GameManager.instance.playersTurn = false;
    }
    protected override void OnCantMove<T>(T component)
    {
        ICombatant targetHit = component as ICombatant;
        if (targetHit != null)
        {
            Attack(targetHit);
        }
        else
        {
            WriteInfoText("You bump into " + component.name);
        }
    }
    protected override void BaseActorUnit_Correspondence(object sender, MessageArgument e)
    {
        WriteInfoText(e.Message);
    }
    protected bool TryChangeLevel(int zDir)
    {
        bool result = false;

        if (zDir != -1 && zDir != 1)
        {
            Debug.Log("Wrong argument for zDir on TryChangeLevel - [" + zDir + "].");
            return result;
        }

        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, 1f, 1 << LayerMask.NameToLayer("Triggers"));
        foreach (Collider2D c2d in colliders)
        {
            if ((c2d.tag == "StairsDown" && zDir == 1) || (c2d.tag == "StairsUp" && zDir == -1))
            {
                StartCoroutine(Restart(zDir));
                enabled = false;

                result = true;
            }
        }

        return result;
    }

    private void Update()
    {
        if (!GameManager.instance.playersTurn) return;

        int horizontal = 0;
        int vertical = 0;
        int zDir = 0;

        horizontal = (int)Input.GetAxisRaw("Horizontal");
        vertical = (int)Input.GetAxisRaw("Vertical");
        zDir = (int)Input.GetAxisRaw("ZLevel");

        if (zDir != 0)
        {
            bool leftShift = Input.GetKey(KeyCode.LeftShift);
            bool rightShift = Input.GetKey(KeyCode.RightShift);

            if (leftShift || rightShift)
            {
                if (TryChangeLevel(zDir))
                {
                    if (zDir == 1)
                        WriteInfoText("You are going down the stairs.");
                    else if (zDir == -1)
                        WriteInfoText("You are going up the stairs.");
                }
                else
                {
                    WriteInfoText("You could not find stairs here!");
                }
            }
        }

        // no diagonal movement
        if (horizontal != 0)
        {
            vertical = 0;
        }

        if (horizontal != 0 || vertical != 0)
        {
            AttemptMove<BaseActorUnit>(horizontal, vertical);
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        switch (collision.tag)
        {
            default:
                Debug.Log(string.Format("OnTriggerEnter2D was encountered; tag: {0}", collision.tag));
                break;
        }
    }
    private IEnumerator Restart(int zDir)
    {
        yield return new WaitForSeconds(restartLevelDelay);
        if (zDir == -1) GameManager.instance.level -= 2;
        SceneManager.LoadScene(0);
    }
    private void WriteInfoText(string message, params object[] args)
    {
        string formatted = string.Format(message, args);
        infoText.text = formatted;
    }
}
